Routes     = require 'models/routes'
Ways       = require 'ways'
Browser    = require 'ways-browser'

module.exports = new class Navigation

	start: ->

		###
		Get the routes
		###
		routes = do Routes

		Ways.use Browser

		Ways.mode 'run+destroy'

		for route in routes

			Ways route.url, route.controller.run, route.controller.destroy

		do Ways.init


	go: ( url, title = null, data = {} ) ->

		Ways.go url, title, data


	silent: ( url ) ->

		Ways.go.silent url