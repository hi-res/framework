AppController = require 'controllers/app_controller'
Siteloader    = require 'controllers/site_loader'
Views         = require 'utils/views'
Model         = require 'models/page'

class PageController extends AppController

	view: null

	run: ( req, done ) ->

		page_id = req.params.id

		@view = Views.render
			view : 'page'
			id   : 'page'
			data : Model.get page_id

		do @view.setup
		do Siteloader.hide
		do done

module.exports = new PageController