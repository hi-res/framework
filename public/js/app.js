/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var App, dictionary, log, settings, siteloader,
	  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

	log = __webpack_require__(1);

	settings = __webpack_require__(2);

	dictionary = __webpack_require__(3);

	siteloader = __webpack_require__(4);

	window.delay = function(delay, func) {
	  return setTimeout(func, delay);
	};

	window.interval = function(delay, func) {
	  return setInterval(func, delay);
	};

	App = (function() {
	  function App() {
	    this.start = __bind(this.start, this);

	    /*
	    		Initial setup
	     */
	    $('body').addClass(settings.browser_class);
	    window.c = log;
	    c.enable = settings.debug;

	    /*
	    		Load dictionary
	     */
	    siteloader.once('shown', (function(_this) {
	      return function() {
	        dictionary.on('load:complete', _this.start);
	        return dictionary.load();
	      };
	    })(this));
	    siteloader.show();
	  }

	  App.prototype.start = function() {
	    var Navigation;
	    $('#main').addClass('loaded');
	    Navigation = __webpack_require__(5);
	    return Navigation.start();
	  };

	  return App;

	})();

	module.exports = new App;


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __slice = [].slice;

	module.exports = {
	  enable: false,
	  clear: function() {
	    if ((typeof console !== "undefined" && console !== null) && (console.clear != null)) {
	      return console.clear();
	    }
	  },
	  log: function() {
	    var args;
	    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	    if (this.enable) {
	      if ((typeof console !== "undefined" && console !== null) && (console.log != null) && (console.log.apply != null)) {
	        return console.log.apply(console, args);
	      } else {
	        return console.log(args);
	      }
	    }
	  },
	  debug: function() {
	    var args;
	    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	    if (this.enable) {
	      if ((typeof console !== "undefined" && console !== null) && (console.debug != null) && (console.debug.apply != null)) {
	        return console.debug.apply(console, args);
	      } else {
	        return console.log(args);
	      }
	    }
	  },
	  info: function() {
	    var args;
	    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	    if (this.enable) {
	      if ((typeof console !== "undefined" && console !== null) && (console.info != null) && (console.info.apply != null)) {
	        return console.info.apply(console, args);
	      } else {
	        return console.log(args);
	      }
	    }
	  },
	  warn: function() {
	    var args;
	    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	    if (this.enable) {
	      if ((typeof console !== "undefined" && console !== null) && (console.warn != null) && (console.warn.apply != null)) {
	        return console.warn.apply(console, args);
	      } else {
	        return console.log(args);
	      }
	    }
	  },
	  error: function() {
	    var args;
	    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	    if (this.enable) {
	      if ((typeof console !== "undefined" && console !== null) && (console.error != null) && (console.error.apply != null)) {
	        return console.error.apply(console, args);
	      } else {
	        return console.log(args);
	      }
	    }
	  }
	};


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var Detect, Settings, md;

	md = new MobileDetect(window.navigator.userAgent);

	Detect = __webpack_require__(6);

	Detect = Detect.Detect;

	Settings = (function() {
	  Settings.prototype.base_path = env.BASE_PATH;

	  Settings.prototype.debug = env.DEBUG;

	  Settings.prototype.language = env.LANGUAGE;

	  Settings.prototype.browser_class = '';

	  Settings.prototype.build = '';

	  Settings.prototype.browser = {
	    id: Detect.browser,
	    version: Detect.version,
	    OS: Detect.OS,
	    retina: window.devicePixelRatio === 2,
	    device_ratio: window.devicePixelRatio
	  };

	  Settings.prototype.platform = {
	    id: '',
	    tablet: false,
	    mobile: false,
	    desktop: false,
	    device: false
	  };

	  function Settings() {
	    var classes;
	    if (md.phone() || md.tablet()) {
	      this.platform.device = true;
	      if (md.tablet()) {
	        this.platform.mobile = false;
	        this.platform.tablet = true;
	        this.platform.id = 'tablet';
	      } else {
	        this.platform.mobile = true;
	        this.platform.tablet = false;
	        this.platform.id = 'mobile';
	      }
	    } else {
	      this.platform.desktop = true;
	      this.platform.id = 'desktop';
	    }
	    classes = [this.platform.id, this.browser.id.toLowerCase() + '_' + this.browser.version];
	    if (this.platform.device) {
	      classes.push('device');
	    }
	    this.browser_class = classes.join(' ');
	    this.browser[this.browser.id] = true;
	  }

	  return Settings;

	})();

	module.exports = new Settings;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var Dictionary, Happens, Loader, Settings;

	Happens = __webpack_require__(10);

	Settings = __webpack_require__(2);

	Loader = __webpack_require__(7);

	Dictionary = (function() {
	  Dictionary.prototype.default_dictionary = 'dictionary.xml';

	  function Dictionary() {
	    Happens(this);
	    this.objs = {};
	  }

	  Dictionary.prototype.load = function() {
	    var loader;
	    loader = new Loader;
	    loader.on('loading', (function(_this) {
	      return function(percent) {
	        return c.info('[ PERCENT LOADED ] ->', percent);
	      };
	    })(this));
	    loader.once('loaded', (function(_this) {
	      return function(manifest) {
	        var asset, _i, _len;
	        for (_i = 0, _len = manifest.length; _i < _len; _i++) {
	          asset = manifest[_i];
	          _this.objs[asset.id] = $(asset.data);
	        }
	        return _this.emit('load:complete');
	      };
	    })(this));
	    loader.add('dictionary.xml', "" + Settings.base_path + "/xml/" + Settings.language + "/dictionary.xml", 'xml');
	    loader.add('routes.xml', "" + Settings.base_path + "/xml/" + Settings.language + "/routes.xml", 'xml');
	    loader.add('data.xml', "" + Settings.base_path + "/xml/" + Settings.language + "/data.xml", 'xml');
	    return loader.load();
	  };

	  Dictionary.prototype.get = function(id, dictionary) {
	    var node;
	    if (dictionary == null) {
	      dictionary = this.default_dictionary;
	    }
	    node = this.get_raw(id, dictionary);
	    if (node) {
	      return node.text();
	    } else {
	      return '';
	    }
	  };

	  Dictionary.prototype.get_raw = function(id, dictionary) {
	    if (dictionary == null) {
	      dictionary = this.default_dictionary;
	    }
	    if (this.objs[dictionary].find(id).length) {
	      return this.objs[dictionary].find(id);
	    }
	    return false;
	  };

	  Dictionary.prototype.get_dictionary = function(dictionary) {
	    if (dictionary == null) {
	      dictionary = this.default_dictionary;
	    }
	    return this.objs[dictionary];
	  };

	  return Dictionary;

	})();

	module.exports = new Dictionary;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var Loader;

	Loader = __webpack_require__(9);

	module.exports = new Loader($('#site_loader'));


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var Browser, Navigation, Routes, Ways;

	Routes = __webpack_require__(8);

	Ways = __webpack_require__(11);

	Browser = __webpack_require__(13);

	module.exports = new (Navigation = (function() {
	  function Navigation() {}

	  Navigation.prototype.start = function() {

	    /*
	    		Get the routes
	     */
	    var route, routes, _i, _len;
	    routes = Routes();
	    Ways.use(Browser);
	    Ways.mode('run+destroy');
	    for (_i = 0, _len = routes.length; _i < _len; _i++) {
	      route = routes[_i];
	      Ways(route.url, route.controller.run, route.controller.destroy);
	    }
	    return Ways.init();
	  };

	  Navigation.prototype.go = function(url, title, data) {
	    if (title == null) {
	      title = null;
	    }
	    if (data == null) {
	      data = {};
	    }
	    return Ways.go(url, title, data);
	  };

	  Navigation.prototype.silent = function(url) {
	    return Ways.go.silent(url);
	  };

	  return Navigation;

	})());


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	var BrowserDetect, browserPrefix;

	BrowserDetect = {
	  init: function() {
	    this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
	    this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
	    return this.OS = this.searchString(this.dataOS) || "an unknown OS";
	  },
	  searchString: function(data) {
	    var dataProp, dataString, i;
	    i = 0;
	    while (i < data.length) {
	      dataString = data[i].string;
	      dataProp = data[i].prop;
	      this.versionSearchString = data[i].versionSearch || data[i].identity;
	      if (dataString) {
	        if (dataString.indexOf(data[i].subString) !== -1) {
	          return data[i].identity;
	        }
	      } else {
	        if (dataProp) {
	          return data[i].identity;
	        }
	      }
	      i++;
	    }
	  },
	  searchVersion: function(dataString) {
	    var index;
	    index = dataString.indexOf(this.versionSearchString);
	    if (index === -1) {
	      return;
	    }
	    return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
	  },
	  dataBrowser: [
	    {
	      string: navigator.userAgent,
	      subString: "Chrome",
	      identity: "Chrome"
	    }, {
	      string: navigator.userAgent,
	      subString: "OmniWeb",
	      versionSearch: "OmniWeb/",
	      identity: "OmniWeb"
	    }, {
	      string: navigator.vendor,
	      subString: "Apple",
	      identity: "Safari",
	      versionSearch: "Version"
	    }, {
	      prop: window.opera,
	      identity: "Opera",
	      versionSearch: "Version"
	    }, {
	      string: navigator.vendor,
	      subString: "iCab",
	      identity: "iCab"
	    }, {
	      string: navigator.vendor,
	      subString: "KDE",
	      identity: "Konqueror"
	    }, {
	      string: navigator.userAgent,
	      subString: "Firefox",
	      identity: "Firefox"
	    }, {
	      string: navigator.vendor,
	      subString: "Camino",
	      identity: "Camino"
	    }, {
	      string: navigator.userAgent,
	      subString: "Netscape",
	      identity: "Netscape"
	    }, {
	      string: navigator.userAgent,
	      subString: "MSIE",
	      identity: "Explorer",
	      versionSearch: "MSIE"
	    }, {
	      string: navigator.userAgent,
	      subString: "Gecko",
	      identity: "Mozilla",
	      versionSearch: "rv"
	    }, {
	      string: navigator.userAgent,
	      subString: "Mozilla",
	      identity: "Netscape",
	      versionSearch: "Mozilla"
	    }
	  ],
	  dataOS: [
	    {
	      string: navigator.platform,
	      subString: "Win",
	      identity: "Windows"
	    }, {
	      string: navigator.platform,
	      subString: "Mac",
	      identity: "Mac"
	    }, {
	      string: navigator.userAgent,
	      subString: "iPhone",
	      identity: "iPhone/iPod"
	    }, {
	      string: navigator.platform,
	      subString: "Linux",
	      identity: "Linux"
	    }
	  ]
	};

	BrowserDetect.init();

	browserPrefix = void 0;

	navigator.sayswho = (function() {
	  var M, N, tem, ua;
	  N = navigator.appName;
	  ua = navigator.userAgent;
	  tem = void 0;
	  M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
	  if (M && ((tem = ua.match(/version\/([\.\d]+)/i)) != null)) {
	    M[2] = tem[1];
	  }
	  M = (M ? [M[1], M[2]] : [N, navigator.appVersion, "-?"]);
	  M = M[0];
	  if (M === "Chrome") {
	    browserPrefix = "webkit";
	  }
	  if (M === "Firefox") {
	    browserPrefix = "moz";
	  }
	  if (M === "Safari") {
	    browserPrefix = "webkit";
	  }
	  if (M === "MSIE") {
	    browserPrefix = "ms";
	  }
	  return browserPrefix;
	})();

	exports.browserPrefix = browserPrefix;

	exports.Detect = BrowserDetect;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var AsyncLoader, BinaryLoader, DataLoader, ImageLoader, happens,
	  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

	happens = __webpack_require__(10);

	DataLoader = __webpack_require__(14);

	BinaryLoader = __webpack_require__(15);

	ImageLoader = __webpack_require__(16);


	/*
	Load files asynchronously
	 */

	module.exports = AsyncLoader = (function() {
	  function AsyncLoader() {
	    this.error = __bind(this.error, this);
	    this.success = __bind(this.success, this);
	    happens(this);
	    this.manifest = [];
	  }

	  AsyncLoader.prototype.add = function(id, file, type, data) {
	    var obj;
	    obj = {
	      id: id,
	      src: file,
	      type: type,
	      data: data
	    };
	    return this.manifest.push(obj);
	  };

	  AsyncLoader.prototype.load = function() {
	    var asset, l, _i, _len, _ref, _results;
	    this.count = 0;
	    this.total = this.manifest.length;
	    this.date = new Date();
	    _ref = this.manifest;
	    _results = [];
	    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	      asset = _ref[_i];
	      switch (asset.type) {
	        case 'json':
	        case 'xml':
	          l = new DataLoader;
	          l.once('loaded', this.success);
	          _results.push(l.load(asset));
	          break;
	        case 'image':
	          l = new ImageLoader;
	          l.once('loaded', this.success);
	          _results.push(l.load(asset));
	          break;
	        case 'binary':
	          l = new BinaryLoader;
	          l.once('loaded', this.success);
	          _results.push(l.load(asset));
	          break;
	        default:
	          _results.push(void 0);
	      }
	    }
	    return _results;
	  };

	  AsyncLoader.prototype.success = function(asset) {
	    this.count++;
	    if (this.count >= this.total) {
	      return this.emit('loaded', this.manifest);
	    }
	  };

	  AsyncLoader.prototype.error = function(error) {
	    return c.log('error', error);
	  };

	  AsyncLoader.prototype.get_asset = function(id) {
	    var asset, result, _i, _len, _ref;
	    result = false;
	    _ref = this.manifest;
	    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	      asset = _ref[_i];
	      if (asset.id.match(id)) {
	        result = asset;
	      }
	    }
	    return result;
	  };

	  return AsyncLoader;

	})();


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	var Dictionary;

	Dictionary = __webpack_require__(3);

	module.exports = function() {
	  var routes, xml;
	  xml = Dictionary.get_dictionary('routes.xml');
	  routes = [];
	  xml.find('route').each((function(_this) {
	    return function(index, item) {
	      var controller, id, obj, route;
	      item = $(item);
	      id = item.attr('id');
	      route = item.attr('route');
	      controller = item.attr('controller');
	      obj = {
	        id: id,
	        url: route,
	        controller: __webpack_require__(12)("./" + controller)
	      };
	      return routes.push(obj);
	    };
	  })(this));
	  return routes;
	};


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var Loader, happens;

	happens = __webpack_require__(10);

	module.exports = Loader = (function() {
	  Loader.prototype.is_loaded = false;

	  function Loader(el) {
	    this.el = el;
	    happens(this);
	  }

	  Loader.prototype.show = function() {
	    var params;
	    this.el.addClass('play');
	    TweenLite.killTweensOf(this.el);
	    params = {
	      autoAlpha: 1,
	      ease: Power2.easeIn,
	      onComplete: (function(_this) {
	        return function() {
	          return _this.emit('shown');
	        };
	      })(this)
	    };
	    return TweenLite.to(this.el, 0.5, params);
	  };

	  Loader.prototype.hide = function() {
	    var params;
	    TweenLite.killTweensOf(this.el);
	    params = {
	      autoAlpha: 0,
	      ease: Power2.easeIn,
	      onComplete: (function(_this) {
	        return function() {
	          return _this.el.removeClass('play');
	        };
	      })(this)
	    };
	    return TweenLite.to(this.el, 0.5, params);
	  };

	  return Loader;

	})();


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Module constructor
	 * @param  {Object} target Target object to extends methods and properties into
	 * @return {Object}        Target after with extended methods and properties
	 */
	module.exports = function(target) {
	  target = target || {};
	  for(var prop in Happens)
	    target[prop] = Happens[prop];
	  return target;
	};



	/**
	 * Class Happens
	 * @type {Object}
	 */
	var Happens = {

	  /**
	   * Initializes event
	   * @param  {String} event Event name to initialize
	   * @return {Array}        Initialized event pool
	   */
	  __init: function(event) {
	    var tmp = this.__listeners || (this.__listeners = []);
	    return tmp[event] || (tmp[event] = []);
	  },

	  /**
	   * Adds listener
	   * @param  {String}   event Event name
	   * @param  {Function} fn    Event handler
	   */
	  on: function(event, fn) {
	    validate(fn);
	    this.__init(event).push(fn);
	  },

	  /**
	   * Removes listener
	   * @param  {String}   event Event name
	   * @param  {Function} fn    Event handler
	   */
	  off: function(event, fn) {
	    var pool = this.__init(event);
	    pool.splice(pool.indexOf(fn), 1);
	  },

	  /**
	   * Add listener the fires once and auto-removes itself
	   * @param  {String}   event Event name
	   * @param  {Function} fn    Event handler
	   */
	  once: function(event, fn) {
	    validate(fn);
	    var self = this, wrapper = function() {
	      self.off(event, wrapper);
	      fn.apply(this, arguments);
	    };
	    this.on(event, wrapper );
	  },

	  /**
	   * Emit some event
	   * @param  {String} event Event name -- subsequent params after `event` will
	   * be passed along to the event's handlers
	   */
	  emit: function(event /*, arg1, arg2 */ ) {
	    var i, pool = this.__init(event).slice(0);
	    for(i in pool)
	      pool[i].apply(this, [].slice.call(arguments, 1));
	  }
	};



	/**
	 * Validates if a function exists and is an instanceof Function, and throws
	 * an error if needed
	 * @param  {Function} fn Function to validate
	 */
	function validate(fn) {
	  if(!(fn && fn instanceof Function))
	    throw new Error(fn + ' is not a Function');
	}

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var way = __webpack_require__(20),
	    flow = __webpack_require__(21);

	var mode, flo, middleware,
	    routes = [];

	dispatch = function(url) {
	  var i, route;
	  url = '/' + url.replace(/^[\/]+|[\/]+$/mg, '');
	  for(i in routes) {
	    route = routes[i];
	    if(routes[i].matcher.test(url))
	      return run(url, routes[i]);
	  }
	  throw new Error("Route not found for url '"+ url +"'");
	};

	run = function(url, route) {
	  if(flo) {
	    flo.run(url, route);
	  } else {
	    route.run(url);
	  }
	  return route;
	};

	module.exports = function(pattern, runner, destroyer, dependency){
	  if(flo && arguments.length < 3)
	    throw new Error('In `flow` mode you must to pass at least 3 args.');
	  
	  var route = way(pattern, runner, destroyer, dependency);
	  routes.push(route);
	  return route;
	};

	exports = module.exports;

	exports.init = function() {
	  dispatch(this.pathname());
	};

	exports.mode = function (m){
	  routes = [];
	  if((mode = m) != null)
	    flo = flow(routes, mode);
	};

	exports.use = function(mid){
	  middleware = new mid;
	  middleware.on('url:change', function() {
	    dispatch(middleware.pathname());
	  });
	};

	exports.pathname = function(){
	  if(middleware)
	    return middleware.pathname();
	};

	exports.go = function(url, title, state){
	  if(middleware)
	    middleware.push(url, title, state);
	  else
	    dispatch(url);
	};

	exports.go.silent = function(url, title, state){
	  if(middleware)
	    middleware.replace(url, title, state);
	};

	exports.reset = function(){
	  flo = null
	  mode = null
	  routes = []
	  middleware = null
	};

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./app_controller": 17,
		"./app_controller.coffee": 17,
		"./index": 18,
		"./index.coffee": 18,
		"./navigation": 5,
		"./navigation.coffee": 5,
		"./page": 19,
		"./page.coffee": 19,
		"./site_loader": 4,
		"./site_loader.coffee": 4
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 12;


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var Event, Hash, History, Index,
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	Event = __webpack_require__(31);

	History = __webpack_require__(22);

	Hash = __webpack_require__(23);

	module.exports = Index = (function(_super) {
	  __extends(Index, _super);

	  Index.prototype.api = null;

	  Index.prototype.base_path = null;

	  function Index() {
	    this.base_path = env.BASE_PATH || window.location.base_path || '';
	    if (window.history.pushState != null) {
	      this.api = new History(this.base_path);
	    } else {
	      this.api = new Hash(this.base_path);
	    }
	    this.api.on('url:change', (function(_this) {
	      return function(pathname) {
	        return _this.emit('url:change', pathname);
	      };
	    })(this));
	    this.history = this.api.history;
	  }

	  Index.prototype.pathname = function() {
	    return this.api.pathname();
	  };

	  Index.prototype.push = function(url, title, state) {
	    return this.api.push(url, title, state);
	  };

	  Index.prototype.replace = function(url, title, state) {
	    return this.api.replace(url, title, state);
	  };

	  return Index;

	})(Event);


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var DataLoader, happens;

	happens = __webpack_require__(10);

	module.exports = DataLoader = (function() {
	  function DataLoader() {
	    happens(this);
	  }

	  DataLoader.prototype.load = function(asset) {
	    return $.ajax({
	      url: asset.src,
	      dataType: asset.type,
	      success: (function(_this) {
	        return function(data) {
	          asset.data = data;
	          return _this.emit('loaded', asset);
	        };
	      })(this),
	      error: (function(_this) {
	        return function(error) {
	          return c.error('error', error);
	        };
	      })(this)
	    });
	  };

	  return DataLoader;

	})();


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var BinaryLoader, happens;

	happens = __webpack_require__(10);

	module.exports = BinaryLoader = (function() {
	  function BinaryLoader() {
	    happens(this);
	  }

	  BinaryLoader.prototype.load = function(asset) {
	    var type, xhr;
	    xhr = this.req();
	    if (!type) {
	      type = "arraybuffer";
	      try {
	        if (Blob.prototype.slice) {
	          type = "blob";
	        }
	      } catch (_error) {}
	    }
	    xhr.open("GET", asset.src, true);
	    xhr.responseType = type;
	    xhr.onprogress = function(e) {};
	    xhr.onreadystatechange = (function(_this) {
	      return function(e) {
	        if (xhr.readyState === 4) {
	          asset.data = xhr.response;
	          _this.emit('loaded', asset);
	          xhr.onreadystatechange = null;
	        }
	      };
	    })(this);
	    return xhr.send(null);
	  };

	  BinaryLoader.prototype.req = function() {
	    if (window.XMLHttpRequest) {
	      return new XMLHttpRequest();
	    }
	    if (window.ActiveXObject) {
	      return new ActiveXObject("MSXML2.XMLHTTP.3.0");
	    }
	  };

	  return BinaryLoader;

	})();


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var ImageLoader, happens;

	happens = __webpack_require__(10);

	module.exports = ImageLoader = (function() {
	  function ImageLoader() {
	    happens(this);
	  }

	  ImageLoader.prototype.load = function(asset) {
	    var image;
	    image = new Image();
	    image.onload = (function(_this) {
	      return function() {
	        asset.data = image;
	        return _this.emit('loaded', asset);
	      };
	    })(this);
	    return image.src = asset.src;
	  };

	  return ImageLoader;

	})();


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	var AppController, happens;

	happens = __webpack_require__(10);

	module.exports = AppController = (function() {
	  function AppController() {
	    happens(this);
	  }

	  AppController.prototype.destroy = function(req, done) {
	    this.view.hide();
	    return this.view.on('destroyed', (function(_this) {
	      return function() {
	        c.warn('[ CONTROLLER DESTROYED ]');
	        _this.view = null;
	        return done();
	      };
	    })(this));
	  };

	  return AppController;

	})();


/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	var AppController, IndexController, Model, Siteloader, Views,
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	AppController = __webpack_require__(17);

	Siteloader = __webpack_require__(4);

	Views = __webpack_require__(24);

	Model = __webpack_require__(26);

	IndexController = (function(_super) {
	  __extends(IndexController, _super);

	  function IndexController() {
	    return IndexController.__super__.constructor.apply(this, arguments);
	  }

	  IndexController.prototype.view = null;

	  IndexController.prototype.run = function(req, done) {
	    this.view = Views.render({
	      view: 'index',
	      id: 'index',
	      data: Model.get()
	    });
	    this.view.setup();
	    Siteloader.hide();
	    return done();
	  };

	  return IndexController;

	})(AppController);

	module.exports = new IndexController;


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	var AppController, Model, PageController, Siteloader, Views,
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	AppController = __webpack_require__(17);

	Siteloader = __webpack_require__(4);

	Views = __webpack_require__(24);

	Model = __webpack_require__(25);

	PageController = (function(_super) {
	  __extends(PageController, _super);

	  function PageController() {
	    return PageController.__super__.constructor.apply(this, arguments);
	  }

	  PageController.prototype.view = null;

	  PageController.prototype.run = function(req, done) {
	    var page_id;
	    page_id = req.params.id;
	    this.view = Views.render({
	      view: 'page',
	      id: 'page',
	      data: Model.get(page_id)
	    });
	    this.view.setup();
	    Siteloader.hide();
	    return done();
	  };

	  return PageController;

	})(AppController);

	module.exports = new PageController;


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var _params_regex = {
	  named: /:\w+/g,
	  splat: /\*\w+/g,
	  optional: /\/(?:\:|\*)(\w+)\?/g
	};

	module.exports = function(pattern, runner, destroyer, dependency) {
	  return new Way(pattern, runner, destroyer, dependency);
	};


	function Way(pattern, runner, destroyer, dependency) {

	  this.matcher = null;
	  this.pattern = pattern;
	  this.runner = runner;
	  this.destroyer = destroyer;
	  this.dependency = dependency;

	  var _params_regex = {
	    named: /:\w+/g,
	    splat: /\*\w+/g,
	    optional: /\/(\:|\*)(\w+)\?/g
	  };

	  if (pattern === '*') {
	    this.matcher = /.*/;
	  } else {
	    this.matcher = pattern.replace(_params_regex.optional, '(?:\/)?$1$2?');
	    this.matcher = this.matcher.replace(_params_regex.named, '([^\/]+)');
	    this.matcher = this.matcher.replace(_params_regex.splat, '(.*?)');
	    this.matcher = new RegExp("^" + this.matcher + "$", 'm');
	  }
	};

	Way.prototype.extract_params = function(url) {
	  var name, names, params, vals, i, len;

	  names = this.pattern.match(/(?::|\*)(\w+)/g);
	  if (names == null) return {};

	  vals = url.match(this.matcher);
	  params = {};
	  for (i = 0, len = names.length; i < len; i++) {
	    name = names[i];
	    params[name.substr(1)] = vals[i+1];
	  }

	  return params;
	};

	Way.prototype.rewrite_pattern = function(pattern, url) {
	  var key, value, reg, params;

	  params = this.extract_params(url);
	  for (key in params) {
	    value = params[key];
	    reg = new RegExp("[\:\*]+" + key, 'g');
	    pattern = pattern.replace(reg, value);
	  }
	  return pattern;
	};

	Way.prototype.computed_dependency = function(url) {
	  return this.rewrite_pattern(this.dependency, url);
	};

	Way.prototype.run = function(url, done) {
	  var req = {
	    url: url,
	    pattern: this.pattern,
	    params: this.extract_params(url)
	  };
	  this.runner(req, done);
	  return req;
	};

	Way.prototype.destroy = function(req, done) {
	  this.destroyer(req, done);
	};

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var happens = __webpack_require__(35),
	    fluid = __webpack_require__(27);

	var find = function(arr, filter) {
	  for (var item, i = 0, len = arr.length; i < len; i++) {
	    if (filter(item = arr[i])) {
	      return item;
	    }
	  }
	};

	var reject = function(arr, filter) {
	  for (var item, copy = [], i = 0, len = arr.length; i < len; i++) {
	    if (!filter(item = arr[i])) {
	      copy.push(item);
	    }
	  }
	  return copy;
	};


	module.exports = function(routes, mode){
	  return new Flow( routes, mode );
	};


	function Flow(routes, mode) {
	  happens(this);

	  this.routes = routes;
	  this.mode = mode;

	  this.deads = [];
	  this.actives = [];
	  this.pendings = [];
	  this.status = 'free'
	}

	Flow.prototype.run = function(url, route) {
	  var flu, self = this;

	  if( this.status == 'busy')
	    this.actives.splice(-1, 1);

	  this.emit('status:busy');

	  this.deads = [];
	  this.pendings = [];

	  flu = fluid(route, url);
	  this.filter_pendings(flu);
	  this.filter_deads();

	  this.status = 'busy';
	  if (this.mode === 'run+destroy') {
	    this.run_pendings(function() {
	      self.destroy_deads(function() {
	        self.status = 'free';
	        self.emit('status:free', self.mode);
	      });
	    });
	  }
	  else if (this.mode === 'destroy+run') {
	    this.destroy_deads(function() {
	      self.run_pendings(function() {
	        self.status = 'free';
	        self.emit('status:free', self.mode);
	      });
	    });
	  }
	};

	Flow.prototype.find_dependency = function(parent) {
	  var route, flu;

	  flu = find(this.actives, function(f) {
	    return f.url === parent.dependency;
	  });
	  if(flu != null) return flu;
	  
	  route = find(this.routes, function(r) {
	    return r.matcher.test(parent.route.dependency);
	  });
	  if(route != null) return fluid(route, parent.dependency);

	  return null;
	};

	Flow.prototype.filter_pendings = function(parent) {
	  var err, flu, route, dep;

	  this.pendings.unshift(parent);
	  if (parent.dependency == null)
	    return;

	  if ((flu = this.find_dependency(parent)) != null)
	    return this.filter_pendings(flu);

	  route = parent.route.pattern;
	  dep = parent.dependency
	  err = "Dependency '" + dep + "' not found for route '" + route + "'";

	  throw new Error(err);
	};

	Flow.prototype.filter_deads = function() {
	  var flu, is_pending, i, len;

	  for (i = 0, len = this.actives.length; i < len; i++) {
	    
	    flu = this.actives[i];
	    is_pending = find(this.pendings, function(f) {
	      return f.url === flu.url;
	    });

	    if (!is_pending) {
	      this.deads.push(flu);
	    }
	  }
	};

	Flow.prototype.run_pendings = function(done) {
	  var flu, is_active, self = this;

	  if (this.pendings.length === 0) return done();

	  flu = this.pendings.shift();
	  is_active = find(this.actives, function(f) {
	    return f.url === flu.url;
	  });

	  if (is_active)
	    return this.run_pendings(done);

	  this.actives.push(flu);
	  this.emit('run:pending', flu.url);

	  flu.run(function() {
	    self.run_pendings(done);
	  });
	};

	Flow.prototype.destroy_deads = function(done) {
	  var flu, self = this;

	  if (this.deads.length === 0) return done();

	  flu = this.deads.pop();
	  this.actives = reject(this.actives, function(f) {
	    return f.url === flu.url;
	  });

	  flu.destroy(function() {
	    self.destroy_deads(done);
	  });
	};

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	var Event, History,
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	Event = __webpack_require__(31);

	module.exports = History = (function(_super) {
	  __extends(History, _super);

	  History.prototype.history = window.history;

	  function History(base_path) {
	    this.base_path = base_path;
	    window.addEventListener('popstate', (function(_this) {
	      return function() {
	        return _this.emit('url:change', _this.pathname());
	      };
	    })(this), false);
	  }

	  History.prototype.pathname = function() {
	    var address, index;
	    if (!this.base_path) {
	      console.warn("doesnt have base_path");
	      return window.location.pathname;
	    } else {
	      index = String(window.location.pathname).indexOf(this.base_path);
	      address = String(window.location.pathname).substr(index);
	      return address = address.replace(this.base_path, '');
	    }
	  };

	  History.prototype.push = function(url, title, state) {
	    window.history.pushState(state, title, url);
	    if (title != null) {
	      document.title = title;
	    }
	    return this.emit('url:change', window.location.pathname);
	  };

	  History.prototype.replace = function(url, title, state) {
	    window.history.replaceState(state, title, url);
	    if (title != null) {
	      return document.title = title;
	    }
	  };

	  return History;

	})(Event);


/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	var Event, Hash, PseudoHistory,
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	Event = __webpack_require__(31);

	PseudoHistory = (function(_super) {
	  __extends(PseudoHistory, _super);

	  function PseudoHistory() {
	    return PseudoHistory.__super__.constructor.apply(this, arguments);
	  }

	  PseudoHistory.prototype.state = null;

	  return PseudoHistory;

	})(Array);

	module.exports = Hash = (function(_super) {
	  __extends(Hash, _super);

	  Hash.prototype.history = null;

	  function Hash(base_path) {
	    var hash, pathname;
	    this.base_path = base_path;
	    this.history = new PseudoHistory;
	    hash = window.location.hash;
	    pathname = window.location.pathname.replace(this.base_path, "");
	    window.attachEvent('onhashchange', (function(_this) {
	      return function() {
	        return _this.emit('url:change', _this.pathname());
	      };
	    })(this), false);
	  }

	  Hash.prototype.pathname = function() {
	    var address, index;
	    if (!this.base_path) {
	      return window.location.hash;
	    } else {
	      index = String(window.location).indexOf(this.base_path);
	      address = String(window.location).substr(index);
	      return address.replace(this.base_path, '');
	    }
	  };

	  Hash.prototype.push = function(url, title, state) {
	    if (this.base_path != null) {
	      if (url.indexOf(this.base_path) !== -1) {
	        url = url.replace(this.base_path, '');
	      }
	    }
	    this.history.push(this.history.state = state);
	    window.location.hash = url;
	    if (title != null) {
	      return document.title = title;
	    }
	  };

	  Hash.prototype.replace = function(url, title, state) {
	    if (this.base_path != null) {
	      if (url.indexOf(this.base_path) !== -1) {
	        url = url.replace(this.base_path, '');
	      }
	    }
	    this.history[this.history.length - 1] = this.history.state = state;
	    if (title != null) {
	      document.title = title;
	    }
	    return window.location.hash.replace(url);
	  };

	  return Hash;

	})(Event);


/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	var dictionary, settings, utils;

	settings = __webpack_require__(2);

	utils = __webpack_require__(28);

	dictionary = __webpack_require__(3);

	module.exports = {
	  render: function(obj) {
	    var View, data, defaults, template, view;
	    defaults = {
	      parent: $('#main'),
	      view: '',
	      id: '',
	      data: []
	    };
	    obj = utils.merge(defaults, obj);
	    template = __webpack_require__(29)("./" + obj.view + ".jade");
	    data = {
	      base_path: settings.base_path,
	      dictionary: dictionary,
	      tablet: settings.platform.tablet,
	      mobile: settings.platform.mobile,
	      data: obj.data
	    };
	    obj.parent.append(template(data));
	    View = __webpack_require__(30)("./" + obj.view);
	    view = new View($("#" + obj.id), data.data);
	    return view;
	  }
	};


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	var Dictionary, Model, PageModel;

	Dictionary = __webpack_require__(3);

	PageModel = (function() {
	  function PageModel() {}

	  PageModel.prototype.index = Number;

	  PageModel.prototype.id = String;

	  PageModel.prototype.url = String;

	  PageModel.prototype.size = String;

	  PageModel.prototype.brand = String;

	  PageModel.prototype.color = String;

	  return PageModel;

	})();

	module.exports = new (Model = (function() {
	  function Model() {
	    var $data, xml;
	    xml = Dictionary.get_dictionary('data.xml');
	    $data = xml.find('page');
	    this.data = [];
	    $data.find('shoe').each((function(_this) {
	      return function(index, item) {
	        var model;
	        item = $(item);
	        model = new PageModel;
	        model.index = index;
	        model.id = item.attr('id');
	        model.url = item.attr('url');
	        model.size = item.find('size').text();
	        model.brand = item.find('brand').text();
	        model.color = item.find('color').text();
	        return _this.data.push(model);
	      };
	    })(this));
	  }

	  Model.prototype.get = function(page_id) {
	    var data, i, id, index, _i, _len, _ref;
	    if (!page_id) {
	      return this.data;
	    }
	    index = 0;
	    _ref = this.data;
	    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
	      data = _ref[i];
	      id = data.url.split('/').pop();
	      if (id === page_id) {
	        return this.data[i];
	      }
	    }
	  };

	  return Model;

	})());


/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var Dictionary, IndexModel, Model;

	Dictionary = __webpack_require__(3);

	IndexModel = (function() {
	  function IndexModel() {}

	  IndexModel.prototype.index = Number;

	  IndexModel.prototype.id = String;

	  IndexModel.prototype.url = String;

	  IndexModel.prototype.name = String;

	  IndexModel.prototype.age = String;

	  IndexModel.prototype.country = String;

	  return IndexModel;

	})();

	module.exports = new (Model = (function() {
	  function Model() {
	    var $data, xml;
	    xml = Dictionary.get_dictionary('data.xml');
	    $data = xml.find('index');
	    this.data = [];
	    $data.find('person').each((function(_this) {
	      return function(index, item) {
	        var model;
	        item = $(item);
	        model = new IndexModel;
	        model.index = index;
	        model.id = item.attr('id');
	        model.url = item.attr('url');
	        model.name = item.find('name').text();
	        model.age = item.find('age').text();
	        model.country = item.find('country').text();
	        return _this.data.push(model);
	      };
	    })(this));
	  }

	  Model.prototype.get = function() {
	    return this.data;
	  };

	  return Model;

	})());


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function(route, url) {
	  return new Fluid(route, url);
	}

	function Fluid(route, url) {
	  this.route = route;
	  this.url = url;

	  if(route.dependency)
	    this.dependency = route.computed_dependency(url);
	}

	Fluid.prototype.run = function(done) {
	  this.req = this.route.run(this.url, done);
	};

	Fluid.prototype.destroy = function(done){
	  if(this.req) this.route.destroy(this.req, done);
	};

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = {
	  lerp: function(min, max, ratio) {
	    return min + ((max - min) * ratio);
	  },
	  random: function(min, max) {
	    return min + Math.random() * (max - min);
	  },
	  radians: function(degrees) {
	    return degrees * (Math.PI / 180);
	  },
	  get_image_from_css_background: function(bg_url) {
	    bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
	    bg_url = (bg_url ? bg_url[2] : "");
	    return bg_url;
	  },

	  /*
	  	https://gist.github.com/svlasov-gists/2383751
	   */
	  merge: function(target, source) {
	    var a, l, property, sourceProperty;
	    if (typeof target !== "object") {
	      target = {};
	    }
	    for (property in source) {
	      if (source.hasOwnProperty(property)) {
	        sourceProperty = source[property];
	        if (typeof sourceProperty === "object") {
	          target[property] = this.merge(target[property], sourceProperty);
	          continue;
	        }
	        target[property] = sourceProperty;
	      }
	    }
	    a = 2;
	    l = arguments.length;
	    while (a < l) {
	      merge(target, arguments[a]);
	      a++;
	    }
	    return target;
	  },

	  /*
	  	Convert hex to rgb
	  	http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
	   */
	  convert_hex_to_rgb: function(hex) {
	    var color, result, shorthandRegex;
	    shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
	      return r + r + g + g + b + b;
	    });
	    result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	    color = false;
	    if (result) {
	      color = [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)];
	    }
	    return color;
	  },
	  calculate_resize: function(el_width, el_height, win_width, win_height) {
	    var image_ratio1, image_ratio2, new_height, new_width, window_ratio, x, y;
	    window_ratio = win_width / win_height;
	    image_ratio1 = el_width / el_height;
	    image_ratio2 = el_height / el_width;
	    if (window_ratio < image_ratio1) {
	      new_height = win_height;
	      new_width = Math.round(new_height * image_ratio1);
	      y = 0;
	      x = (win_width * .5) - (new_width * .5);
	    } else {
	      new_width = win_width;
	      new_height = Math.round(new_width * image_ratio2);
	      y = (win_height * .5) - (new_height * .5);
	      x = 0;
	    }
	    return {
	      x: x,
	      y: y,
	      width: new_width,
	      height: new_height
	    };
	  },
	  fullscreen_resize: function($el, el_width, el_height, win_width, win_height, cssbackground) {
	    var data;
	    if (cssbackground == null) {
	      cssbackground = false;
	    }
	    data = this.calculate_resize(el_width, el_height, win_width, win_height);
	    if (cssbackground) {
	      return $el.css({
	        'background-size': "" + data.width + "px " + data.height + "px",
	        'background-position': "" + data.x + "px " + data.y + "px",
	        'width': "" + data.width + "px",
	        'height': "" + data.height + "px"
	      });
	    } else {
	      return $el.css({
	        'margin-top': "" + data.y + "px",
	        'margin-left': "" + data.x + "px",
	        'width': "" + data.width + "px",
	        'height': "" + data.height + "px"
	      });
	    }
	  }
	};


/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./index.jade": 36,
		"./page.jade": 37
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 29;


/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./app_view": 32,
		"./app_view.coffee": 32,
		"./index": 33,
		"./index.coffee": 33,
		"./page": 34,
		"./page.coffee": 34,
		"./partials/loader": 9,
		"./partials/loader.coffee": 9
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 30;


/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	// Generated by CoffeeScript 1.6.3
	var Happens,
	  __slice = [].slice;

	module.exports = Happens = (function() {
	  function Happens() {}

	  Happens.prototype.on = function(key, callback) {
	    var pool;
	    pool = this.__listeners || (this.__listeners = []);
	    return (pool[key] || (pool[key] = [])).push(callback);
	  };

	  Happens.prototype.off = function(key, callback) {
	    var pool, _ref;
	    if ((pool = (_ref = this.__listeners) != null ? _ref[key] : void 0) != null) {
	      return pool.splice(pool.indexOf(callback), 1);
	    }
	  };

	  Happens.prototype.once = function(key, callback) {
	    var wrapper,
	      _this = this;
	    return this.on(key, wrapper = function() {
	      _this.off(key, wrapper);
	      return callback.apply(_this, arguments);
	    });
	  };

	  Happens.prototype.emit = function() {
	    var args, key, listener, pool, _i, _len, _ref, _ref1, _results;
	    key = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
	    if ((pool = (_ref = this.__listeners) != null ? _ref[key] : void 0) != null) {
	      _ref1 = pool.slice(0);
	      _results = [];
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        listener = _ref1[_i];
	        _results.push(listener.apply(this, args));
	      }
	      return _results;
	    }
	  };

	  Happens.mixin = function(target) {
	    var prop, _results;
	    _results = [];
	    for (prop in this.prototype) {
	      _results.push(target[prop] = this.prototype[prop]);
	    }
	    return _results;
	  };

	  return Happens;

	})();


/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var AppView, happens, navigation, settings;

	happens = __webpack_require__(10);

	navigation = __webpack_require__(5);

	settings = __webpack_require__(2);

	module.exports = AppView = (function() {
	  AppView.prototype.animating = false;

	  function AppView(el) {
	    this.el = el;
	    happens(this);
	    this.route_links();
	  }

	  AppView.prototype.route_links = function() {
	    return this.el.find('a[href*="/"]').each((function(_this) {
	      return function(index, item) {
	        return $(item).click(function(event) {
	          var url;
	          url = $(event.currentTarget).attr('href');
	          if (!_this.animating) {
	            navigation.go("" + settings.base_path + url);
	          }
	          return false;
	        });
	      };
	    })(this));
	  };

	  AppView.prototype.show = function() {
	    var params;
	    this.animating = true;
	    TweenLite.killTweensOf(this.el);
	    params = {
	      autoAlpha: 1,
	      ease: Power3.easeOut,
	      onComplete: (function(_this) {
	        return function() {
	          return _this.animating = false;
	        };
	      })(this)
	    };
	    return TweenLite.to(this.el, 1, params);
	  };

	  AppView.prototype.hide = function() {
	    var params;
	    TweenLite.killTweensOf(this.el);
	    params = {
	      autoAlpha: 0,
	      ease: Power3.easeOut,
	      onComplete: (function(_this) {
	        return function() {
	          return _this.destroy();
	        };
	      })(this)
	    };
	    return TweenLite.to(this.el, 1, params);
	  };

	  AppView.prototype.destroy = function() {
	    this.el.remove();
	    return this.emit('destroyed');
	  };

	  return AppView;

	})();


/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var AppView, Index,
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	AppView = __webpack_require__(32);

	module.exports = Index = (function(_super) {
	  __extends(Index, _super);

	  function Index(el, data) {
	    this.el = el;
	    this.data = data;
	    Index.__super__.constructor.call(this, this.el, this.data);
	    c.debug('[ DATA INDEX ]', this.data);
	  }

	  Index.prototype.setup = function() {
	    return this.show();
	  };

	  return Index;

	})(AppView);


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var AppView, Page,
	  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

	AppView = __webpack_require__(32);

	module.exports = Page = (function(_super) {
	  __extends(Page, _super);

	  function Page(el, data) {
	    this.el = el;
	    this.data = data;
	    this.setup = __bind(this.setup, this);
	    Page.__super__.constructor.call(this, this.el, this.data);
	    if (!this.data) {
	      alert('[ Not a valid URL ]');
	    }
	    c.debug('[ DATA PAGE ]', this.data);
	  }

	  Page.prototype.setup = function() {
	    return this.show();
	  };

	  return Page;

	})(AppView);


/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function(target) {
	  for(var prop in Happens)
	    target[prop] = Happens[prop];
	  return target;
	};

	function validate(fn) {
	  if(!(fn && fn instanceof Function))
	    throw new Error(fn + 'is not a function');
	}

	var Happens = {
	  __init: function(event) {
	    var tmp = this.__listeners || (this.__listeners = []);
	    return tmp[event] || (tmp[event] = []);
	  },

	  on: function(event, fn) {
	    validate(fn);
	    this.__init(event).push(fn);
	  },

	  off: function(event, fn) {
	    var pool = this.__init(event);
	    pool.splice(pool.indexOf(fn), 1);
	  },

	  once: function(event, fn) {
	    validate(fn);
	    var self = this, wrapper = function() {
	      self.off(event, wrapper);
	      fn.apply(this, arguments);
	    };
	    this.on(event, wrapper );
	  },

	  emit: function(event) {
	    var i, pool = this.__init(event).slice(0);
	    for(i in pool)
	      pool[i].apply(this, [].slice.call(arguments, 1));
	  }
	};

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(38);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;

	buf.push("<div id=\"index\" class=\"page\"><div class=\"content _soho_reg\"><h1>" + (jade.escape((jade_interp = locals.dictionary.get('index')) == null ? '' : jade_interp)) + "</h1><a href=\"/page\">PAGE</a></div></div>");;return buf.join("");
	}

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(38);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;

	buf.push("<div id=\"page\" class=\"page\"><div class=\"content _soho_reg\"><h1>" + (jade.escape((jade_interp = locals.dictionary.get('page')) == null ? '' : jade_interp)) + "</h1><a href=\"/\">HOME</a></div></div>");;return buf.join("");
	}

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	/**
	 * Merge two attribute objects giving precedence
	 * to values in object `b`. Classes are special-cased
	 * allowing for arrays and merging/joining appropriately
	 * resulting in a string.
	 *
	 * @param {Object} a
	 * @param {Object} b
	 * @return {Object} a
	 * @api private
	 */

	exports.merge = function merge(a, b) {
	  if (arguments.length === 1) {
	    var attrs = a[0];
	    for (var i = 1; i < a.length; i++) {
	      attrs = merge(attrs, a[i]);
	    }
	    return attrs;
	  }
	  var ac = a['class'];
	  var bc = b['class'];

	  if (ac || bc) {
	    ac = ac || [];
	    bc = bc || [];
	    if (!Array.isArray(ac)) ac = [ac];
	    if (!Array.isArray(bc)) bc = [bc];
	    a['class'] = ac.concat(bc).filter(nulls);
	  }

	  for (var key in b) {
	    if (key != 'class') {
	      a[key] = b[key];
	    }
	  }

	  return a;
	};

	/**
	 * Filter null `val`s.
	 *
	 * @param {*} val
	 * @return {Boolean}
	 * @api private
	 */

	function nulls(val) {
	  return val != null && val !== '';
	}

	/**
	 * join array as classes.
	 *
	 * @param {*} val
	 * @return {String}
	 */
	exports.joinClasses = joinClasses;
	function joinClasses(val) {
	  return Array.isArray(val) ? val.map(joinClasses).filter(nulls).join(' ') : val;
	}

	/**
	 * Render the given classes.
	 *
	 * @param {Array} classes
	 * @param {Array.<Boolean>} escaped
	 * @return {String}
	 */
	exports.cls = function cls(classes, escaped) {
	  var buf = [];
	  for (var i = 0; i < classes.length; i++) {
	    if (escaped && escaped[i]) {
	      buf.push(exports.escape(joinClasses([classes[i]])));
	    } else {
	      buf.push(joinClasses(classes[i]));
	    }
	  }
	  var text = joinClasses(buf);
	  if (text.length) {
	    return ' class="' + text + '"';
	  } else {
	    return '';
	  }
	};

	/**
	 * Render the given attribute.
	 *
	 * @param {String} key
	 * @param {String} val
	 * @param {Boolean} escaped
	 * @param {Boolean} terse
	 * @return {String}
	 */
	exports.attr = function attr(key, val, escaped, terse) {
	  if ('boolean' == typeof val || null == val) {
	    if (val) {
	      return ' ' + (terse ? key : key + '="' + key + '"');
	    } else {
	      return '';
	    }
	  } else if (0 == key.indexOf('data') && 'string' != typeof val) {
	    return ' ' + key + "='" + JSON.stringify(val).replace(/'/g, '&apos;') + "'";
	  } else if (escaped) {
	    return ' ' + key + '="' + exports.escape(val) + '"';
	  } else {
	    return ' ' + key + '="' + val + '"';
	  }
	};

	/**
	 * Render the given attributes object.
	 *
	 * @param {Object} obj
	 * @param {Object} escaped
	 * @return {String}
	 */
	exports.attrs = function attrs(obj, terse){
	  var buf = [];

	  var keys = Object.keys(obj);

	  if (keys.length) {
	    for (var i = 0; i < keys.length; ++i) {
	      var key = keys[i]
	        , val = obj[key];

	      if ('class' == key) {
	        if (val = joinClasses(val)) {
	          buf.push(' ' + key + '="' + val + '"');
	        }
	      } else {
	        buf.push(exports.attr(key, val, false, terse));
	      }
	    }
	  }

	  return buf.join('');
	};

	/**
	 * Escape the given string of `html`.
	 *
	 * @param {String} html
	 * @return {String}
	 * @api private
	 */

	exports.escape = function escape(html){
	  var result = String(html)
	    .replace(/&/g, '&amp;')
	    .replace(/</g, '&lt;')
	    .replace(/>/g, '&gt;')
	    .replace(/"/g, '&quot;');
	  if (result === '' + html) return html;
	  else return result;
	};

	/**
	 * Re-throw the given `err` in context to the
	 * the jade in `filename` at the given `lineno`.
	 *
	 * @param {Error} err
	 * @param {String} filename
	 * @param {String} lineno
	 * @api private
	 */

	exports.rethrow = function rethrow(err, filename, lineno, str){
	  if (!(err instanceof Error)) throw err;
	  if ((typeof window != 'undefined' || !filename) && !str) {
	    err.message += ' on line ' + lineno;
	    throw err;
	  }
	  try {
	    str = str || __webpack_require__(39).readFileSync(filename, 'utf8')
	  } catch (ex) {
	    rethrow(err, null, lineno)
	  }
	  var context = 3
	    , lines = str.split('\n')
	    , start = Math.max(lineno - context, 0)
	    , end = Math.min(lines.length, lineno + context);

	  // Error context
	  var context = lines.slice(start, end).map(function(line, i){
	    var curr = i + start + 1;
	    return (curr == lineno ? '  > ' : '    ')
	      + curr
	      + '| '
	      + line;
	  }).join('\n');

	  // Alter exception message
	  err.path = filename;
	  err.message = (filename || 'Jade') + ':' + lineno
	    + '\n' + context + '\n\n' + err.message;
	  throw err;
	};


/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {



/***/ }
/******/ ])