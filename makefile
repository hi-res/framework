setup:
	git config core.fileMode false
	make cli_setup
	npm install

watch:
	NODE_ENV=development gulp

release:
	NODE_ENV=production gulp build

images_watch:
	node_modules/.bin/live-spritesheet -c assets/images/config.json

cli_compile:
	shjs cli/build.coffee
	
cli_setup:
	make cli_compile
	cd cli && sudo npm install -g