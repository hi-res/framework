Dictionary = require 'models/dictionary'

class PageModel

	index : Number
	id 		: String
	url   : String
	size  : String
	brand : String
	color : String
	
module.exports = new class Model

	constructor: -> 

		xml = Dictionary.get_dictionary 'data.xml'

		$data  = xml.find 'page'
		@data  = []
	
		$data.find( 'shoe' ).each ( index, item ) =>

			item = $ item
		
			model       = new PageModel
			model.index = index
			model.id    = item.attr 'id'
			model.url   = item.attr 'url'
			model.size  = item.find('size').text()
			model.brand = item.find('brand').text()
			model.color = item.find('color').text()

			@data.push model


	get: ( page_id ) ->

		unless page_id then return @data

		index = 0

		for data, i in @data

			id = data.url.split('/').pop()

			return @data[ i ] if id is page_id

