#!/usr/bin/env node
(function() {
  var CLI, fs, program,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  program = require('commander');

  fs = require('fs');

  CLI = (function() {
    CLI.prototype.view = null;

    function CLI() {
      this.generate_data = __bind(this.generate_data, this);
      this.commands();
    }

    CLI.prototype.commands = function() {
      program.version('0.1.1');
      program.command('gen [view]').description('Generate a new View & Route').action((function(_this) {
        return function(view) {
          return _this.generate_view(view);
        };
      })(this));
      program.command('del [view]').description('Delete an existing View & Route').action((function(_this) {
        return function(view) {
          return _this.delete_view(view);
        };
      })(this));
      program.command('*').action(function(env) {
        return console.log(env + ' is not a valid command...');
      });
      return program.parse(process.argv);
    };


    /*
    	GENERATE VIEW
     */

    CLI.prototype.generate_view = function(view) {
      if (!view) {
        return console.log('Please give your view a name: app gen [view_name]');
      }
      this.view = view.toLowerCase();
      fs.open('src/coffee/models/' + this.view + '.coffee', 'w');
      fs.open('src/coffee/views/' + this.view + '.coffee', 'w');
      fs.open('src/coffee/controllers/' + this.view + '.coffee', 'w');
      fs.open('src/stylus/sections/' + this.view + '.styl', 'w');
      fs.open('src/templates/' + this.view + '.jade', 'w');
      fs.readFile('cli/mvc/model.coffee', (function(_this) {
        return function(err, data) {
          return fs.writeFile("src/coffee/models/" + _this.view + ".coffee", _this.generate_data(data));
        };
      })(this));
      fs.readFile('cli/mvc/controller.coffee', (function(_this) {
        return function(err, data) {
          return fs.writeFile("src/coffee/controllers/" + _this.view + ".coffee", _this.generate_data(data));
        };
      })(this));
      fs.readFile('cli/mvc/view.coffee', (function(_this) {
        return function(err, data) {
          return fs.writeFile("src/coffee/views/" + _this.view + ".coffee", _this.generate_data(data));
        };
      })(this));
      fs.writeFile("src/stylus/sections/" + this.view + ".styl", this.stylus_data());
      fs.writeFile("src/templates/" + this.view + ".jade", this.jade_data());
      fs.readFile('public/xml/en/routes.xml', (function(_this) {
        return function(err, data) {
          data = data.toString().replace('</routes>', '');
          fs.writeFile('public/xml/en/routes.xml', data);
          return fs.appendFile('public/xml/en/routes.xml', _this.route_data());
        };
      })(this));
      return console.log('Generated ' + view + ' view');
    };

    CLI.prototype.generate_data = function(data) {
      return data.toString().replace(/<view>/g, this.camelCase());
    };

    CLI.prototype.camelCase = function() {
      return this.view.replace(/_(.)/g, function(x, chr) {
        return chr.toUpperCase();
      });
    };


    /*
    	DELETE VIEW
     */

    CLI.prototype.delete_view = function(view) {
      if (!view) {
        return console.log('Please pass the name of the view you wish to delete: app del [view_name]');
      }
      this.view = view.toLowerCase();
      fs.unlink('src/coffee/models/' + this.view + '.coffee');
      fs.unlink('src/coffee/controllers/' + this.view + '.coffee');
      fs.unlink('src/coffee/views/' + this.view + '.coffee');
      fs.unlink('src/stylus/sections/' + this.view + '.styl');
      fs.unlink('src/templates/' + this.view + '.jade');
      fs.readFile('public/xml/en/routes.xml', (function(_this) {
        return function(err, data) {
          var route;
          route = '<route id="' + _this.view + '" route="/' + _this.view + '" controller="' + _this.view + '" />';
          data = data.toString().replace(route, '');
          return fs.writeFile('public/xml/en/routes.xml', data);
        };
      })(this));
      return console.log('Deleted ' + this.view + ' view');
    };


    /*
    	TEMPLATES
     */

    CLI.prototype.route_data = function() {
      var data;
      data = '	<route id="' + this.view + '" route="/' + this.view + '" controller="' + this.view + '" /> \r\n \r\n</routes>';
      return data;
    };

    CLI.prototype.jade_data = function() {
      var data;
      data = "#" + this.view + ".page \r\n \r\n	.content \r\n		h1 " + this.view + " page";
      return data;
    };

    CLI.prototype.stylus_data = function() {
      var data;
      data = "@import '../_mixins' \r\n \r\nbody.desktop \r\n \r\n	#" + this.view + " \r\n		.content \r\n			h1 \r\n				font-size: 30px \r\n				text-align: center \r\n				margin-top: 30px";
      return data;
    };

    return CLI;

  })();

  module.exports = new CLI;

}).call(this);
