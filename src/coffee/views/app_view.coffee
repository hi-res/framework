happens    = require 'happens'
navigation = require 'controllers/navigation'
settings   = require 'utils/settings'

module.exports = class AppView

	animating: false

	constructor: ( @el ) ->
		
		happens @

		do @route_links

	route_links: ->

		# route links starting with "/"
		@el.find( 'a[href*="/"]' ).each ( index, item ) =>
			
			$( item ).click ( event ) =>

				url = $( event.currentTarget ).attr 'href'
				
				navigation.go "#{settings.base_path}#{url}" unless @animating
				
				return off

	show: ->

		@animating = true

		TweenLite.killTweensOf @el 

		params = 
			autoAlpha: 1
			ease: Power3.easeOut
			onComplete: => @animating = false

		TweenLite.to @el, 1, params

	hide: ->

		TweenLite.killTweensOf @el

		params = 
			autoAlpha: 0
			ease: Power3.easeOut
			onComplete: => do @destroy

		TweenLite.to @el, 1, params

	destroy: ->

		do @el.remove

		@emit 'destroyed'
