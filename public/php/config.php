<?php

switch ($_SERVER['HTTP_HOST']) {
	
	case 'example.com':
	case 'www.example.com':
		$env 	   = 'live';
		$BASE_PATH = '';
		break;

	default:
		$env 	   = 'dev';
		$BASE_PATH = '/framework';
	
}