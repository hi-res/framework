md     = new MobileDetect window.navigator.userAgent
Detect = require 'utils/detect'
Detect = Detect.Detect

class Settings

	base_path     : env.BASE_PATH # root path
	debug         : env.DEBUG     # [Boolean]
	language      : env.LANGUAGE  # Language
	browser_class : ''            # Chrome / Safari / Firefox
	build         : ''            # mobile / desktop

	browser:
		id           : Detect.browser                 # ID [String]
		version      : Detect.version                 # Version [String]
		OS           : Detect.OS                      # OS [String]
		retina       : (window.devicePixelRatio is 2) # Retina supported [Boolean]
		device_ratio : window.devicePixelRatio        # Device ratio [Number]

	platform:
		id      : ''    # The platform [String] 
		tablet  : false # Is a tablet? [Boolean]
		mobile  : false # Is a mobile? [Boolean]
		desktop : false # Is desktop? Set after the class definition [Boolean]
		device  : false # Is a tablet or mobile? [Boolean]

	constructor: ->

		if md.phone() or md.tablet()

			@platform.device = true

			if md.tablet()
				@platform.mobile = false
				@platform.tablet = true
				@platform.id     = 'tablet'
			else
				@platform.mobile = true
				@platform.tablet = false
				@platform.id     = 'mobile'

		else
			@platform.desktop = true
			@platform.id      = 'desktop'

		classes = [ @platform.id, @browser.id.toLowerCase() + '_' + @browser.version ]

		classes.push 'device' if @platform.device

		@browser_class = classes.join ' '

		@browser[ @browser.id ] = true


module.exports = new Settings