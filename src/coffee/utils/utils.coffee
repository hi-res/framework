module.exports = 

	lerp: ( min, max, ratio ) ->
		min + ((max - min) * ratio)
	
	random: ( min, max ) ->
		min + Math.random() * (max - min)

	radians: ( degrees ) ->
		degrees * (Math.PI / 180)

	get_image_from_css_background: ( bg_url ) ->

		# ^ Either "none" or url("...urlhere..")
		bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url)
		bg_url = (if bg_url then bg_url[2] else "") # If matched, retrieve url, otherwise ""
		return bg_url

	###
	https://gist.github.com/svlasov-gists/2383751
	###
	merge: (target, source) ->
		
		# Merges two (or more) objects,
		# giving the last one precedence 
		
		target = {}  if typeof target isnt "object"
		
		for property of source
			if source.hasOwnProperty(property)
				sourceProperty = source[property]
				if typeof sourceProperty is "object"
					target[property] = @merge(target[property], sourceProperty)
					continue
				target[property] = sourceProperty
		a = 2
		l = arguments.length

		while a < l
			merge target, arguments[a]
			a++
		
		target


	###
	Convert hex to rgb
	http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
	###
	convert_hex_to_rgb: ( hex ) ->

		# Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
		hex = hex.replace(shorthandRegex, (m, r, g, b) ->
			r + r + g + g + b + b
		)
		result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)

		color = false

		if result
			color = [
				parseInt(result[1], 16)
				parseInt(result[2], 16)
				parseInt(result[3], 16)
			]

		return color

	calculate_resize: ( el_width, el_height, win_width, win_height ) ->

		window_ratio = win_width / win_height
		image_ratio1 = el_width / el_height
		image_ratio2 = el_height / el_width

		if window_ratio < image_ratio1
			
			new_height = win_height
			new_width  = Math.round( new_height * image_ratio1 )

			y = 0
			x = (win_width * .5) - (new_width * .5) 

		else
			
			new_width  = win_width
			new_height = Math.round( new_width * image_ratio2 );

			y = (win_height * .5) - (new_height * .5)
			x = 0 

		return {
			x      : x
			y      : y
			width  : new_width
			height : new_height
		}

	fullscreen_resize: ( $el, el_width, el_height, win_width, win_height, cssbackground = false ) ->

		data = @calculate_resize el_width, el_height, win_width, win_height

		if cssbackground

			$el.css
				'background-size'     : "#{data.width}px #{data.height}px"
				'background-position' : "#{data.x}px #{data.y}px"
				'width'       		  : "#{data.width}px"
				'height'      		  : "#{data.height}px"

		else
		
			$el.css
				'margin-top'  : "#{data.y}px"
				'margin-left' : "#{data.x}px"
				'width'       : "#{data.width}px"
				'height'      : "#{data.height}px"

