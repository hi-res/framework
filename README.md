# Hi-ReS! framework

For simple web apps

# Setting up

Run ```make setup```

Create a symlink like below from your localhost:

```
framework -> /Users/hires/Desktop/work/hires/framework/public
```

# Cli

Framework comes bundled with a cli for easily creating and deleting views

Create

```
app gen <view>
```

Remove

```
app del <view>
```