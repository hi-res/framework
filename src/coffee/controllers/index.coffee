AppController = require 'controllers/app_controller'
Siteloader    = require 'controllers/site_loader'
Views         = require 'utils/views'
Model         = require 'models/index'

class IndexController extends AppController

	view: null

	run: ( req, done ) ->

		@view = Views.render
			view : 'index'
			id   : 'index'
			data : do Model.get

		do @view.setup
		do Siteloader.hide
		do done

module.exports = new IndexController