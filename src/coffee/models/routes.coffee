Dictionary = require 'models/dictionary'

module.exports = ->

	xml = Dictionary.get_dictionary 'routes.xml'

	routes = []

	xml.find( 'route' ).each ( index, item ) =>

		item = $ item

		id         = item.attr 'id'
		route      = item.attr 'route'
		controller = item.attr 'controller'

		obj = 
			id 		     : id
			url        : route
			controller : require "../controllers/#{controller}"

		routes.push obj

	return routes