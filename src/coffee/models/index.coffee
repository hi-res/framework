Dictionary = require 'models/dictionary'

class IndexModel

	index   : Number
	id 		  : String
	url     : String
	name    : String
	age     : String
	country : String

module.exports = new class Model

	constructor: -> 

		xml = Dictionary.get_dictionary 'data.xml'

		$data  = xml.find 'index'
		@data  = []
	
		$data.find( 'person' ).each ( index, item ) =>

			item = $ item
		
			model         = new IndexModel
			model.index   = index
			model.id      = item.attr 'id'
			model.url     = item.attr 'url'
			model.name    = item.find('name').text()
			model.age     = item.find('age').text()
			model.country = item.find('country').text()

			@data.push model


	get: -> return @data
