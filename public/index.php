<?php 

require_once( 'php/config.php' );
require_once( 'php/dictionary.php' );

$LANGUAGE = 'en';

$dictionary = new Dictionary();
$dictionary->load(
	array(
		'default' => 'xml/'.$LANGUAGE.'/dictionary.xml',
		)
);

?>

<!doctype html>
<html>

	<head>

		<title><?php echo $dictionary->get('meta_title'); ?></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<meta property="og:image"       content="http://link-to-og-image"/>
   	<meta property="og:title"       content="<?php echo $dictionary->get('meta_title'); ?>"/>
    <meta property="og:description" content="<?php echo $dictionary->get('meta_description'); ?>"/>

		<script>
			var env = {
				BASE_PATH : "<?php echo $BASE_PATH ?>",
				LANGUAGE  : "<?php echo $LANGUAGE ?>",
				DEBUG     : true
			};
		</script>

		<link href="<?php echo $BASE_PATH ?>/css/app.css"         rel="stylesheet" type="text/css" >
		<link href="<?php echo $BASE_PATH ?>/css/spritesheet.css" rel="stylesheet" type="text/css">

		<?php include( 'php/styles.php' ); ?>

		<!-- Libraries -->
		<script src="<?php echo $BASE_PATH ?>/js/vendor/modernizr.custom.js"></script>
		<script src="<?php echo $BASE_PATH ?>/js/vendor/CSSPlugin.min.js"></script>
		<script src="<?php echo $BASE_PATH ?>/js/vendor/jquery-2.1.1.min.js"></script>
		<script src="<?php echo $BASE_PATH ?>/js/vendor/TweenLite.js"></script>
		<script src="<?php echo $BASE_PATH ?>/js/vendor/mobile-detect.min.js"></script>

	</head>

	<body>

		<header id="header">
			<span class="spritesheet logo layer layer_1"></span>
		</header>

		<main id="main"></main>

		<div id="site_loader" class="loader layer layer_1" style="visibility: hidden; opacity: 0;">
			<div class="spritesheet spin_black"></div>
		</div>

		<script type='text/javascript'>
			var s = document.createElement('script'),
			h = document.getElementsByTagName('head')[0];
			s.src = '<?php echo $BASE_PATH ?>/js/app.js';
			h.insertBefore(s, h.lastChild);
		</script>

	</body>

</html>