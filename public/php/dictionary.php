<?php

class Dictionary 
{
	private $xmls;

	function __construct() {
		$this->xmls = array();
	}


	/**
	 * Load an array of dictionary xml files
	 * @param $file
	 */
	public function load($files){
		
		foreach( $files as $id => $file ) {
			if (file_exists($file)) {

				$this->xmls[ $id ] = simplexml_load_file($file);		 
			} else {
				exit('Failed to open -> '.$file);
			}	
		}
		
	}

	/**
	 * Get a dictionary value from key and the id of the dictionary
	 * @param $key
	 * @param $dictionary
	 * @return $string
	 */
	public function get($key, $dictionary = "default" ){
		if( isset( $this->xmls[ $dictionary ] ) ){
			return (string) $this->xmls[ $dictionary ]->$key;	
		}
		return false;
	}

	public function label($key, $dictionary = "default" ){
		if( isset( $this->xmls[ $dictionary ] ) ){
			return (string) "<span class='label_".$key."'>" . $this->xmls[ $dictionary ]->$key . "</span>";	
		}
		return false;
	}

	public function get_config_item($env, $key, $dictionary = "default" ){
		if( isset( $this->xmls[ $dictionary ] ) ){
			return (string) $this->xmls[ $dictionary ]->$env->$key;	
		}
		return false;
	}

}

?>