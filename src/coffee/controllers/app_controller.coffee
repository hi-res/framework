happens = require 'happens'

module.exports = class AppController

	constructor: -> 

		happens @

	destroy: ( req, done ) ->

		do @view.hide

		@view.on 'destroyed', =>

			c.warn '[ CONTROLLER DESTROYED ]'

			@view = null

			do done