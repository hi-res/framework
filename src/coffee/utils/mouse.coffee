happens  = require 'happens'
win 	 = require './window'
settings = require '../settings'

module.exports = class Mouse

	mouse_x: 0
	mouse_y: 0

	mouse_last_x: 0
	mouse_last_y: 0

	normal_x: 0
	normal_y: 0

	normal_center_x: 0.1
	normal_center_y: 0.1

	clicked_x: 0
	clicked_y: 0

	relative_x: 0
	relative_y: 0

	enabled: on

	constructor: ( @el ) ->

		happens @

		@body = $ 'body'


	bind: ->

		if settings.platform.device

			# @el.on 'touchstart', @mousedown
			# @el.on 'touchend', @mouseup
			# @el.on 'touchmove', @mousemove

			window.ondevicemotion = @on_device_motion

		else

			@el.on 'mousedown', @mousedown
			@el.on 'mouseup', @mouseup
			@el.on 'mousemove', @mousemove

	unbind: ->

		if settings.platform.device

			window.ondevicemotion = ->

			# @el.off 'touchstart', @mousedown
			# @el.off 'touchend', @mouseup
			# @el.off 'touchmove', @mousemove

		else

			@el.off 'mousedown', @mousedown
			@el.off 'mouseup', @mouseup
			@el.off 'click', @click


	mousemove: ( event ) =>

		@mouse_last_x = @mouse_x
		@mouse_last_y = @mouse_y

		position = @_get_event_position event

		@mouse_x = position.x
		@mouse_y = position.y

		@normal_x = @mouse_x / win.width
		@normal_y = @mouse_y / win.height

		@normal_center_x = -0.5 + @normal_x
		@normal_center_y = -0.5 + @normal_y

		@relative_x = @mouse_x - @clicked_x
		@relative_y = @mouse_y - @clicked_y

		@emit 'move'

	on_device_motion: (e) =>
	
		switch window.orientation
			when -90
				# A / landscape (button at left)
				accelerationX = e.accelerationIncludingGravity.y
				accelerationY = e.accelerationIncludingGravity.x

			when 90
				# B / landscape (button at right)
				accelerationX = -e.accelerationIncludingGravity.y
				accelerationY = -e.accelerationIncludingGravity.x

			when 0
				# C / portrait (button at bottom)
				accelerationX = e.accelerationIncludingGravity.x
				accelerationY = -e.accelerationIncludingGravity.y

			when 180
				# D / portrait (button at top)
				accelerationX = -e.accelerationIncludingGravity.x
				accelerationY = e.accelerationIncludingGravity.y

		pageX = win.width  * 0.5 + (accelerationX * 0.25) * win.width
		pageY = win.height * 0.5 + (accelerationY * 0.25) * win.height
		
		pageX = Math.max pageX, 0
		pageX = Math.min pageX, win.width

		pageY = Math.max pageY, 0
		pageY = Math.min pageY, win.height

		# Normalise

		@mouse_x = pageX
		@mouse_y = pageY

		@normal_x = @mouse_x / win.width
		@normal_y = @mouse_y / win.height

		@normal_center_x = -0.5 + @normal_x
		@normal_center_y = -0.5 + @normal_y

		@normal_center_x = parseFloat @normal_center_x.toFixed 5
		@normal_center_y = parseFloat @normal_center_y.toFixed 5

		# c.log @normal_center_x, @normal_center_y

		@emit 'move'

	mousedown: ( event ) => 

		if settings.platform.device
			event.preventDefault()

		position = @_get_event_position event

		@clicked_x = position.x
		@clicked_y = position.y

		@relative_x = 0
		@relative_y = 0

		@emit 'down', position

	mouseup: ( event ) => 

		@emit 'up'

	###
	Get the touch / mouse position and return the coords
	@param  {Object} event
	@return {Object}
	###

	_get_event_position: ( event ) ->

		if settings.platform.device
			evt_x = event.originalEvent.changedTouches[0].clientX
			evt_y = event.originalEvent.changedTouches[0].clientY
		else
			evt_x = event.pageX
			evt_y = event.pageY
		
		return {
			x : evt_x
			y : evt_y
		}