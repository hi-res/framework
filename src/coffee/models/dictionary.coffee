Happens   = require 'happens'
Settings  = require 'utils/settings'
Loader    = require 'utils/loading/async_loader'

class Dictionary

	default_dictionary: 'dictionary.xml'

	constructor: ->

		Happens @

		@objs = {}

	
	load: ->

		loader = new Loader

		loader.on 'loading', ( percent )  => c.info '[ PERCENT LOADED ] ->', percent

		loader.once 'loaded', ( manifest ) =>

			@objs[ asset.id ] = $ asset.data for asset in manifest

			@emit 'load:complete'
	
		loader.add 'dictionary.xml', "#{Settings.base_path}/xml/#{Settings.language}/dictionary.xml", 'xml'
		loader.add 'routes.xml',     "#{Settings.base_path}/xml/#{Settings.language}/routes.xml",     'xml'
		loader.add 'data.xml',       "#{Settings.base_path}/xml/#{Settings.language}/data.xml",       'xml'
		
		do loader.load


	get: ( id, dictionary = @default_dictionary ) ->
		
		node = @get_raw id, dictionary

		if node then return node.text() else return ''


	get_raw: ( id, dictionary = @default_dictionary ) ->

		if @objs[ dictionary ].find( id ).length
			return @objs[ dictionary ].find( id )

		return false


	get_dictionary: ( dictionary = @default_dictionary ) ->
		
		return @objs[ dictionary ]


module.exports = new Dictionary