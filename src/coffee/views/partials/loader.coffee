happens = require 'happens'

module.exports = class Loader

	is_loaded: off

	constructor: ( @el ) ->

		happens @

	show: ->

		@el.addClass 'play'

		TweenLite.killTweensOf @el

		params =
			autoAlpha: 1
			ease: Power2.easeIn
			onComplete: => @emit 'shown'

		TweenLite.to @el, 0.5, params


	hide: ->

		TweenLite.killTweensOf @el

		params =
			autoAlpha: 0
			ease: Power2.easeIn
			onComplete: => @el.removeClass 'play'

		TweenLite.to @el, 0.5, params