#!/usr/bin/env node

program = require 'commander'
fs      = require 'fs'

class CLI
	
	view: null

	constructor: ->

		do @commands

	commands: ->

		program.version '0.1.1'

		# Generate a new View & Route
		program
			.command( 'gen [view]' )
			.description( 'Generate a new View & Route' )
			.action ( view ) => @generate_view( view )

		# Delete a specific View and Route
		program
			.command( 'del [view]' )
			.description( 'Delete an existing View & Route' )
			.action ( view ) => @delete_view( view )

		# Fired if command is not recognized
		program
			.command( '*' )
			.action ( env ) ->
				console.log env + ' is not a valid command...'

		program.parse process.argv

	###
	GENERATE VIEW
	###
	generate_view: ( view ) ->

		# Only continue if the views name is actually passed
		unless view then return console.log 'Please give your view a name: app gen [view_name]'
		
		@view = view.toLowerCase()

		# Generate files needed for the new View
		fs.open 'src/coffee/models/'      + @view + '.coffee', 'w'
		fs.open 'src/coffee/views/'       + @view + '.coffee', 'w'
		fs.open 'src/coffee/controllers/' + @view + '.coffee', 'w'
		fs.open 'src/stylus/sections/'    + @view + '.styl'  , 'w'
		fs.open 'src/templates/'          + @view + '.jade'  , 'w'

		# Generate Controller from mvc template
		fs.readFile 'cli/mvc/model.coffee', ( err, data ) =>
			fs.writeFile "src/coffee/models/#{@view}.coffee", @generate_data data

		# Generate Model from mvc template
		fs.readFile 'cli/mvc/controller.coffee', ( err, data ) =>
			fs.writeFile "src/coffee/controllers/#{@view}.coffee", @generate_data data
		
		# Generate View from mvc template
		fs.readFile 'cli/mvc/view.coffee', ( err, data ) =>
			fs.writeFile "src/coffee/views/#{@view}.coffee", @generate_data data
		
		# Generate Stylus from data below
		fs.writeFile "src/stylus/sections/#{@view}.styl", do @stylus_data
		
		# Generate Jade from data below
		fs.writeFile "src/templates/#{@view}.jade", do @jade_data

		# Read the routes.xml file
		fs.readFile 'public/xml/en/routes.xml', ( err, data ) =>
				
			# Remove last line ( to be reinserted later )
			data = data.toString().replace '</routes>', ''
			
			# Write in the new data with removed line to the file
			fs.writeFile 'public/xml/en/routes.xml', data
			
			# Append the new Route to the routes.xml file and reinsert the last line
			fs.appendFile 'public/xml/en/routes.xml', do @route_data
		
		# Success Log
		console.log 'Generated ' + view + ' view'

	generate_data: ( data ) =>
		# Replace keyword in template with View name and convert to camelCase
		return data.toString().replace /<view>/g, @camelCase()

	camelCase: ->
		# Make string Camel Case because it's nice...
		return @view.replace /_(.)/g, ( x, chr ) -> return chr.toUpperCase()

	###
	DELETE VIEW
	###
	delete_view: ( view ) ->

		# Only continue if the views name is actually passed
		unless view then return console.log 'Please pass the name of the view you wish to delete: app del [view_name]'
			
		@view = view.toLowerCase()

		# Delete all the files that match the View passed from the command line
		fs.unlink 'src/coffee/models/'      + @view + '.coffee'
		fs.unlink 'src/coffee/controllers/' + @view + '.coffee'
		fs.unlink 'src/coffee/views/'       + @view + '.coffee'
		fs.unlink 'src/stylus/sections/'    + @view + '.styl'
		fs.unlink 'src/templates/'          + @view + '.jade'

		# Read the data from the routes.xml file
		fs.readFile 'public/xml/en/routes.xml', ( err, data ) =>

			# Get the Route that matches the given View and remove it from the file
			route = '<route id="' + @view + '" route="/' + @view + '" controller="' + @view + '" />'
			data  = data.toString().replace route, ''
			
			fs.writeFile 'public/xml/en/routes.xml', data

		# Success Log
		console.log 'Deleted ' + @view + ' view'

	###
	TEMPLATES
	###
	route_data: ->

		data = '	<route id="' + @view + '" route="/' + @view + '" controller="' + @view + '" />
		\r\n
		\r\n</routes>
		'
		return data

	jade_data: ->

		data = "##{@view}.page
			\r\n
			\r\n	.content
			\r\n		h1 #{@view} page
		"
		return data

	stylus_data: ->

		data = "@import '../_mixins'
			\r\n
			\r\nbody.desktop
			\r\n
			\r\n	##{@view}
			\r\n		.content
			\r\n			h1
			\r\n				font-size: 30px
			\r\n				text-align: center
			\r\n				margin-top: 30px
		"
		return data

module.exports = new CLI