settings   = require 'utils/settings'
utils      = require 'utils/utils'
dictionary = require 'models/dictionary'

module.exports = 

	render: ( obj ) ->

		defaults = 
			parent : $('#main')
			view   : '' 
			id     : '' 
			data   : []

		obj = utils.merge defaults, obj

		template = require "templates/#{obj.view}.jade"

		data =
			base_path  : settings.base_path
			dictionary : dictionary
			tablet     : settings.platform.tablet
			mobile     : settings.platform.mobile
			data       : obj.data

		obj.parent.append template data

		View = require "views/#{obj.view}"
		view = new View $("##{obj.id}"), data.data

		return view