BrowserDetect =
	init: ( ) ->
		@browser = @searchString(@dataBrowser) or "An unknown browser"
		@version = @searchVersion(navigator.userAgent) or @searchVersion(navigator.appVersion) or "an unknown version"
		@OS = @searchString(@dataOS) or "an unknown OS"

	searchString: (data) ->
		i = 0

		while i < data.length
			dataString = data[i].string
			dataProp = data[i].prop
			@versionSearchString = data[i].versionSearch or data[i].identity
			if dataString
				return data[i].identity  unless dataString.indexOf(data[i].subString) is -1
			else return data[i].identity  if dataProp
			i++
		return

	searchVersion: (dataString) ->
		index = dataString.indexOf(@versionSearchString)
		return  if index is -1
		parseFloat dataString.substring(index + @versionSearchString.length + 1)

	dataBrowser: [
		{
			string: navigator.userAgent
			subString: "Chrome"
			identity: "Chrome"
		}
		{
			string: navigator.userAgent
			subString: "OmniWeb"
			versionSearch: "OmniWeb/"
			identity: "OmniWeb"
		}
		{
			string: navigator.vendor
			subString: "Apple"
			identity: "Safari"
			versionSearch: "Version"
		}
		{
			prop: window.opera
			identity: "Opera"
			versionSearch: "Version"
		}
		{
			string: navigator.vendor
			subString: "iCab"
			identity: "iCab"
		}
		{
			string: navigator.vendor
			subString: "KDE"
			identity: "Konqueror"
		}
		{
			string: navigator.userAgent
			subString: "Firefox"
			identity: "Firefox"
		}
		{
			string: navigator.vendor
			subString: "Camino"
			identity: "Camino"
		}
		{
			# for newer Netscapes (6+)
			string: navigator.userAgent
			subString: "Netscape"
			identity: "Netscape"
		}
		{
			string: navigator.userAgent
			subString: "MSIE"
			identity: "Explorer"
			versionSearch: "MSIE"
		}
		{
			string: navigator.userAgent
			subString: "Gecko"
			identity: "Mozilla"
			versionSearch: "rv"
		}
		{
			# for older Netscapes (4-)
			string: navigator.userAgent
			subString: "Mozilla"
			identity: "Netscape"
			versionSearch: "Mozilla"
		}
	]
	dataOS: [
		{
			string: navigator.platform
			subString: "Win"
			identity: "Windows"
		}
		{
			string: navigator.platform
			subString: "Mac"
			identity: "Mac"
		}
		{
			string: navigator.userAgent
			subString: "iPhone"
			identity: "iPhone/iPod"
		}
		{
			string: navigator.platform
			subString: "Linux"
			identity: "Linux"
		}
	]

BrowserDetect.init()

browserPrefix = undefined

navigator.sayswho = (->
	N = navigator.appName
	ua = navigator.userAgent
	tem = undefined
	M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i)
	M[2] = tem[1]  if M and (tem = ua.match(/version\/([\.\d]+)/i))?
	M = (if M then [
		M[1]
		M[2]
	] else [
		N
		navigator.appVersion
		"-?"
	])
	M = M[0]
	browserPrefix = "webkit"  if M is "Chrome"
	browserPrefix = "moz"  if M is "Firefox"
	browserPrefix = "webkit"  if M is "Safari"
	browserPrefix = "ms"  if M is "MSIE"
	return browserPrefix
)()

exports.browserPrefix = browserPrefix
exports.Detect 		  = BrowserDetect