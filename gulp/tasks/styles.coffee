gulp        = require 'gulp'
livereload  = require 'gulp-livereload'
stylus      = require 'gulp-stylus'
nib         = require 'nib'
CSSmin      = require 'gulp-minify-css'
prefix      = require 'gulp-autoprefixer'
concat      = require 'gulp-concat'
gulpif      = require 'gulp-if'
handleError = require '../util/handle_error'

production  = process.env.NODE_ENV is 'production'
development = process.env.NODE_ENV is 'development'

exports.paths =
	source      : './src/stylus/app.styl'
	watch       : './src/stylus/**/*.styl'
	destination : './public/css/'

gulp.task 'styles', ->

	gulp.src exports.paths.source

		.pipe stylus use: nib()
		.pipe gulpif production, CSSmin()
		.pipe gulp.dest exports.paths.destination
		.pipe gulpif development, livereload()

		.on 'error', handleError