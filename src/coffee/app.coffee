log        = require 'utils/log'
settings   = require 'utils/settings'
dictionary = require 'models/dictionary'
siteloader = require 'controllers/site_loader'

window.delay    = ( delay, func ) -> setTimeout  func, delay
window.interval = ( delay, func ) -> setInterval func, delay

class App

	constructor: ->

		###
		Initial setup
		###
		$('body').addClass settings.browser_class

		window.c = log
		c.enable = settings.debug

		###
		Load dictionary
		###
		siteloader.once 'shown', =>

			dictionary.on 'load:complete', @start
			do dictionary.load

		do siteloader.show


	start: =>

		$('#main').addClass 'loaded'

		Navigation = require "controllers/navigation"

		do Navigation.start

	
module.exports = new App