AppController = require 'controllers/app_controller'
Siteloader    = require 'controllers/site_loader'
Views         = require 'utils/views'
Model         = require 'models/<view>'

class <view>Controller extends AppController

	run: ( req, done ) ->

		console.log "[ <view> controller ]"

		@view = Views.render
			view : '<view>'
			id   : '<view>'
			data : []

		do @view.setup
		do Siteloader.hide
		do done

module.exports = new <view>Controller